export function createSpinner(data,duration) {
	var m_d = 86400000;
	var defaultData = defaultSpinnerData();
	for (var i = data.length - 1; i >= 0; i--) {
		let free = (data[i].duration-duration)/15;
		let start = Number(data[i].start_time)%m_d/900000;
		let start_hour = start/4;
		let start_15 = start%4;
		for(let i=0;i<free;i++){
			if(start_15==4){
				start_hour++;
				start_15=0;
			}
			defaultData.set(start_hour*100+start_15*15);
			start_15++;
		}
	}
}

function defaultSpinnerData(){
	var data = new Map();
	for(var i=9;i<18;i++){
		data.set(i*100,'不可约');
		data.set(i*100+15,'不可约');
		data.set(i*100+30,'不可约');
		data.set(i*100+45,'不可约');
	}
	console.info(data);
}


function test(){
	var data = new Array();
	var duration = 60;
	data[0] = {
		duration:120,
		start_time:1533200400000
	}
	data[1] = {
		duration:120,
		start_time:1533213000000
	}
	var day = 86400000;
	var defaultData = new Map();
	for(var i=9;i<18;i++){
		defaultData.set(i*100,'不可约');
		defaultData.set(i*100+15,'不可约');
		defaultData.set(i*100+30,'不可约');
		defaultData.set(i*100+45,'不可约');
	}
	for (var i = data.length - 1; i >= 0; i--) {
		let free = (data[i].duration-duration)/15;
		let start = Number(data[i].start_time)%day/900000;
		console.info(start);
		let start_hour = parseInt(start/4);
		let start_15 = start%4;
		for(let i=0;i<free;i++){
			if(start_15>=4){
				start_hour++;
				start_15-=4;
			}
			console.info(start_hour+':'+start_15);
			defaultData.set(start_hour*100+start_15*15,'可约');
			start_15++;
		}
	}
	console.info(defaultData);

}