import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'api/v1/org/customer',
    method: 'get',
    params: query
  })
}

export function fetchCustomer(id) {
  return request({
    url: 'api/v1/org/customer',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: 'api/v1/org/customer',
    method: 'get',
    params: { pv }
  })
}

export function createCustomer(data) {
  return request({
    url: 'api/v1/org/customer',
    method: 'post',
    data
  })
}

export function updateStatus(id,status) {
  var data ={
    id ,
    status
  }
  return request({
    url: 'api/v1/org/customer',
    method: 'put',
    data
  })
}

export function updateCustomer(data) {
  return request({
    url: 'api/v1/org/customer',
    method: 'put',
    data
  })
}
