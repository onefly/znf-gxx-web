import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'api/v1/org/employee',
    method: 'get',
    params: query
  })
}
export function fetchEmployee(id) {
  return request({
    url: 'api/v1/org/employee',
    method: 'get',
    params: { id }
  })
}
export function getFirstVerifyEmployees() {
  return request({
    url: '/api/v1/stat/employee_statistics',
    method: 'get'
  })
}

export function fetchPv(pv) {
  return request({
    url: 'api/v1/org/employee',
    method: 'get',
    params: { pv }
  })
}

export function createEmployee(data) {
  return request({
    url: 'api/v1/org/employee/create_with_user',
    method: 'post',
    data
  })
}

export function updateStatus(id, status) {
  var data = {
    id,
    status
  }
  return request({
    url: 'api/v1/org/employee',
    method: 'put',
    data
  })
}

export function updateEmployee(data) {
  return request({
    url: 'api/v1/org/employee',
    method: 'put',
    data
  })
}
