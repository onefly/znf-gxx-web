import request from '@/utils/request'

export function fetchDepartmentList(query) {
  return request({
    url: 'api/v1/org/department',
    method: 'get',
    params: query
  })
}

export function createDepartment(data) {
  return request({
    url: 'api/v1/org/department',
    method: 'post',
    data
  })
}

export function updateDepartment(data) {
  return request({
    url: 'api/v1/org/department',
    method: 'put',
    data
  })
}

export function deleteDepartment(id) {
    return request({
      url: 'api/v1/org/department?id='+id,
      method: 'delete',
    })
  }
