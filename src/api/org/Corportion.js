import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'api/v1/org/corporation',
    method: 'get',
    params: query
  })
}

export function deleteCorporation(id) {
  return request({
    url: 'api/v1/org/corporation/'+id,
    method: 'delete',
  })
}

export function fetchCorporationList() {
  return request({
    url: 'api/v1/org/corporation',
    method: 'get'
  })
}
export function fetchPv(pv) {
  return request({
    url: 'api/v1/org/corporation',
    method: 'get',
    params: { pv }
  })
}
export function createCorporation(data) {
  return request({
    url: 'api/v1/org/corporation/create',
    method: 'post',
    data
  })
}

export function updateStatus(id,status) {
  var data ={
    id ,
    status
  }
  return request({
    url: 'api/v1/org/corporation',
    method: 'put',
    data
  })
}

export function updateCorporation(data) {
  return request({
    url: 'api/v1/org/corporation',
    method: 'put',
    data
  })
}
