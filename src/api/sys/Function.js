import request from '@/utils/request'

export function fetchAllFunction() {
  return request({
    url: 'api/v1/sys/function/all',
    method: 'get'
  })
}


