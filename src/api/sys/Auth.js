import request from '@/utils/request'

export function resetPwdByOldPwd(data) {
  return request({
    url: 'api/v1/sys/user/reset_by_oldpwd',
    method: 'post',
    params: data
  })
}
export function getQiniuUploadToken() {
  return request({
    url: 'api/v1/qinniu/oss/upload_token',
    method: 'get'
  })
}
