import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'api/v1/sys/role',
    method: 'get',
    params: query
  })
}
export function fetchAllRole() {
  return request({
    url: 'api/v1/sys/role/all',
    method: 'get'
  })
}
export function fetchRole(id) {
  return request({
    url: 'api/v1/sys/role',
    method: 'get',
    params: { id }
  })
}
export function fetchPv(pv) {
  return request({
    url: 'api/v1/sys/role',
    method: 'get',
    params: { pv }
  })
}

export function createRole(data) {
  return request({
    url: 'api/v1/sys/role',
    method: 'post',
    data
  })
}

export function updateStatus(id,status) {
  var data ={
    id ,
    status
  }
  return request({
    url: 'api/v1/sys/role',
    method: 'put',
    data
  })
}
export function modifyFunction(id,data) {
  return request({
    url: 'api/v1/sys/role/'+id+'/function',
    method: 'put',
    data
  })
}

export function updateRole(data) {
  return request({
    url: 'api/v1/sys/role',
    method: 'put',
    data
  })
}

