import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'api/v1/busi/scoremoney',
    method: 'get',
    params: query
  })
}

export function fetchScoreMoney(id) {
  return request({
    url: 'api/v1/busi/scoremoney',
    method: 'get',
    params: { id }
  })
}

export function fetchScoreMoneyList() {
  return request({
    url: 'api/v1/busi/scoremoney',
    method: 'get'
  })
}
export function fetchPv(pv) {
  return request({
    url: 'api/v1/busi/scoremoney',
    method: 'get',
    params: { pv }
  })
}
export function createScoreMoney(data) {
  return request({
    url: 'api/v1/busi/scoremoney',
    method: 'post',
    data
  })
}

export function updateStatus(id,status) {
  var data ={
    id ,
    status
  }
  return request({
    url: 'api/v1/busi/scoremoney',
    method: 'put',
    data
  })
}

export function updateScoreMoney(data) {
  return request({
    url: 'api/v1/busi/scoremoney',
    method: 'put',
    data
  })
}
