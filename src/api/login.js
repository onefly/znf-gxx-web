import request from '@/utils/request'
import Qs from 'qs'
export function loginByUsername(username, password,corpCode) {
  const data = Qs.stringify({
    username,
    password,corpCode
  })
  return request({
    url: '/auth/admin/login',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/auth/admin/logout/',
    method: 'post'
  })
}

export function getUserInfo(token) {
  return request({
    url: '/api/v1/sys/user/info',
    method: 'get'
    })
}
