import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'api/v1/shop/shop',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: 'api/v1/shop/shop',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: 'api/v1/shop/shop',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: 'api/v1/shop/shop',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: 'api/v1/shop/shop/',
    method: 'put',
    data
  })
}

export function updateStatus(id,status) {
  var data ={
    id ,
    status
  }
  return request({
    url: 'api/v1/shop/shop/',
    method: 'put',
    data
  })
}
