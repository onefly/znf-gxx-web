import Cookies from 'js-cookie'

const app = {
  state: {
    sidebar: {
      opened: !+Cookies.get('sidebarStatus'),
      withoutAnimation: false
    },
    device: 'desktop',
    language: Cookies.get('language') || 'en',
    applyCount: 0,
    assignCount: 0,
    firstVerifyCount: 0,
    secondVerifyCount: 0
  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1)
      } else {
        Cookies.set('sidebarStatus', 0)
      }
      state.sidebar.opened = !state.sidebar.opened
      state.sidebar.withoutAnimation = false
    },
    CLOSE_SIDEBAR: (state, withoutAnimation) => {
      Cookies.set('sidebarStatus', 1)
      state.sidebar.opened = false
      state.sidebar.withoutAnimation = withoutAnimation
    },
    TOGGLE_DEVICE: (state, device) => {
      state.device = device
    },
    SET_LANGUAGE: (state, language) => {
      state.language = language
      Cookies.set('language', language)
    },
    ADD_COUNT: (state, data) => {
      if (data === 'new_apply') {
        ++state.applyCount
      } else if (data === 'new_assign') {
        ++state.assignCount
      } else if (data === 'new_second') {
        ++state.secondVerifyCount
      } else if (data === 'new_first') {
        ++state.firstVerifyCount
      }
    },
    RESET_EVENT: (state, data) => {
      if (data === 'new_apply') {
        state.applyCount = 0
      } else if (data === 'new_assign') {
        state.assignCount = 0
      } else if (data === 'new_second') {
        state.secondVerifyCount = 0
      } else if (data === 'new_first') {
        state.firstVerifyCount = 0
      }
    }
  },
  actions: {
    toggleSideBar({ commit }) {
      commit('TOGGLE_SIDEBAR')
    },
    closeSideBar({ commit }, { withoutAnimation }) {
      commit('CLOSE_SIDEBAR', withoutAnimation)
    },
    toggleDevice({ commit }, device) {
      commit('TOGGLE_DEVICE', device)
    },
    setLanguage({ commit }, language) {
      commit('SET_LANGUAGE', language)
    },
    addEvent({ commit }, data) {
      commit('ADD_COUNT', data)
    },
    resetEvent({ commit }, data) {
      commit('RESET_EVENT', data)
    }
  }
}

export default app
