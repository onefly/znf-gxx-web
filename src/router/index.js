import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/views/layout/Layout'

export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/authredirect', component: () => import('@/views/login/authredirect'), hidden: true },
  { path: '/404', component: () => import('@/views/errorPage/404'), hidden: true },
  { path: '/401', component: () => import('@/views/errorPage/401'), hidden: true },
  {
    path: '/',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      name: 'dashboard',
      meta: { title: 'dashboard', icon: 'dashboard', noCache: true }
    }]
  }]
export default new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
export const asyncRouterMap = [
  {
    path: '/shop',
    component: Layout,
    name: '组织管理',
    meta: {
      title: '组织管理',
      icon: 'table',
      roles: ['admin']
    },
    children: [
      { path: '/org-employee', component: () => import('@/views/org/Employee'), name: 'employeeManager', meta: { title: '员工管理', roles: ['admin'] }},
      { path: '/org-corporation', component: () => import('@/views/org/Corporation'), name: 'corporationManager', meta: { title: '公司管理', roles: ['admin'] }},
      { path: '/org-department', component: () => import('@/views/org/Department'), name: 'departmentManager', meta: { title: '部门管理', roles: ['admin'] }},
    ]
  },
  {
    path: '/busi',
    component: Layout,
    name: '业务管理',
    meta: {
      title: '业务管理',
      icon: 'table',
      roles: ['admin', 'fv']
    },
    children: [
    ]
  },
  {
    path: '/sys',
    component: Layout,
    name: '系统管理',
    meta: {
      title: '系统管理',
      icon: 'table',
      roles: ['admin']
    },
    children: [
      { path: '/sys-role', component: () => import('@/views/sys/Role'), name: 'RoleManager', meta: { title: '角色管理', roles: ['admin'] }},
    ]
  },
  {
    path: '/task',
    component: Layout,
    name: '任务管理',
    meta: {
      title: '任务管理',
      icon: 'table',
      roles: ['admin']
    },
    children: [
      { path: '/task-TodoList', component: () => import('@/views/task/TodoList'), name: 'todoListManager', meta: { title: '我的待办', roles: ['admin'] }},
      { path: '/task-HistoryList', component: () => import('@/views/task/HistoryList'), name: 'historyManager', meta: { title: '我的已办', roles: ['admin'] }},
    ]
  },
  {
    path: '/error',
    component: Layout,
    redirect: 'noredirect',
    name: 'errorPages',
    meta: {
      title: 'errorPages',
      icon: '404'
    },
    hidden: true,
    children: [
      { path: '401', component: () => import('@/views/errorPage/401'), name: 'page401', meta: { title: 'page401', hidden: true, noCache: true }},
      { path: '404', component: () => import('@/views/errorPage/404'), name: 'page404', meta: { title: 'page404', hidden: true, noCache: true }}
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]
